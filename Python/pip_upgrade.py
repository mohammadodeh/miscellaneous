'''
*
* Automatic upgrade of all PIP packages
*
* VERSION: 1.0.2
*   - ADDED   : Initial release.
*   - FIXED   : Successful upgrades counter counting more
*               packages than it should.
*   - FIXED   : Account for failure *while* building.
*   - ADDED   : Color coded output to make life easier.
*
*
* KNOWN ISSUES:
*   - Not tested on Windows
*
*
* AUTHOR                    :   Mohammad Odeh
* DATE                      :   Dec. 26th, 2018 Year of Our Lord
* LAST CONTRIBUTION DATE    :   Jan.  2nd, 2019 Year of Our Lord
*
'''

try:
    from    pexpect                 import  spawn               # Call external programs (UNIX)
except:
    from    pexpect.popen_spawn     import  PopenSpawn as spawn # Call external programs (Windows)

from        argparse                import  ArgumentParser      # Add input arguments to script
from        platform                import  system              # Running platform info
from        sys                     import  version_info        # Get version of python running script
from        os                      import  geteuid             # Check if running with root privilege
from        time                    import  time                # Time statistics

# ************************************************************************
# =====================> CONSTRUCT ARGUMENT PARSER <=====================*
# ************************************************************************

ap = ArgumentParser()

# Print out stuff to help debug
string = "WARNING: Prints EVERYTHING!!"
ap.add_argument( "-v"   , "--verbose"   ,
                 dest   = "verbose"     ,
                 action = 'store_true'  , default=False ,
                 help   = "{}".format(string)           )

args = ap.parse_args()

##args.verbose    = True
# ************************************************************************
# ===========================> PROGRAM  SETUP <==========================*
# ************************************************************************
class str_format:
    CEND      = '\33[0m'
    CBOLD     = '\33[1m'
    CITALIC   = '\33[3m'
    CURL      = '\33[4m'
    CBLINK    = '\33[5m'
    CBLINK2   = '\33[6m'
    CSELECTED = '\33[7m'

    CBLACK  = '\33[30m'
    CRED    = '\33[31m'
    CGREEN  = '\33[32m'
    CYELLOW = '\33[33m'
    CBLUE   = '\33[34m'
    CVIOLET = '\33[35m'
    CBEIGE  = '\33[36m'
    CWHITE  = '\33[37m'

    CBLACKBG  = '\33[40m'
    CREDBG    = '\33[41m'
    CGREENBG  = '\33[42m'
    CYELLOWBG = '\33[43m'
    CBLUEBG   = '\33[44m'
    CVIOLETBG = '\33[45m'
    CBEIGEBG  = '\33[46m'
    CWHITEBG  = '\33[47m'

    CGREY    = '\33[90m'
    CRED2    = '\33[91m'
    CGREEN2  = '\33[92m'
    CYELLOW2 = '\33[93m'
    CBLUE2   = '\33[94m'
    CVIOLET2 = '\33[95m'
    CBEIGE2  = '\33[96m'
    CWHITE2  = '\33[97m'

    CGREYBG    = '\33[100m'
    CREDBG2    = '\33[101m'
    CGREENBG2  = '\33[102m'
    CYELLOWBG2 = '\33[103m'
    CBLUEBG2   = '\33[104m'
    CVIOLETBG2 = '\33[105m'
    CBEIGEBG2  = '\33[106m'
    CWHITEBG2  = '\33[107m'

# --------------------------

class pip_upgrade( object ):
    def __init__( self ):
        '''
        Can do this to make sure we are running a
        specific version of Python,
        
        if( version_info >= (3,0,0) ):
            Do something here...
        elif( version_info >= (2,0,0) ):
            Do something else...
        '''
        if( system() == 'Linux' and geteuid() != 0 ):                   # Check that we are running as root
            string = "You need to have root privileges to run this script.\n"
            string = "{}Please try again, this time using 'sudo -H'.\n".format( string )
            string = "{}{}Exitting.{}".format( str_format.CRED, string, str_format.CEND )
            print( string ); quit()
        
        self.ver = version_info[0]                                      # Get running python version
        
        if  ( system() == 'Linux'   ):                                  # Construct command for Linux
            self.pip_cmd = 'pip{}'.format(self.ver)                     #   ...
        elif( system() == 'Windows' ):                                  # Construct command for Windows
            self.pip_cmd = 'py -{} pip'.format(self.ver)                #   ...
        else:                                                           # Else, it is neither
            print( "{} OS not supported".format(system()) )             #   Quit program
            quit()                                                      #   ...

        string = "[INFO] Python{} DETECTED RUNNING ON {} OS".format(self.ver, system())
        string = "{}{}{}".format( str_format.CYELLOW, string, str_format.CEND )
        print( "=" * len( string ) ); print( "{}".format(string) ); print( "=" * len( string ) )

        self.package_ctr, self.up2date_ctr = 0, 0
        self.failure_ctr, self.success_ctr = 0, 0
        self.start_timer = time()
        
        self.run()

        self.stats()

# --------------------------

    def run( self ):
        '''
        Spawns a process and extracts all pip packages names
        '''
        cmd     = "{} freeze".format( self.pip_cmd )                    # Construct command
        package = spawn( cmd, timeout=None )                            # Spawn child process
        
        for name in package:                                            # Read STDOUT of spawned child
            out = name.decode('utf-8').strip('\r\n')                    #   Process
            out = out.split('=')[0]                                     #   Extract package name
            string = "[INFO] Found: {:_^{width}}".format( out, width=len(out)+10 )
            print( "{}{}{}".format(str_format.CBLUE, string, str_format.CEND) )

            self.package_ctr += 1
            self.upgrade_packages( out )

        if( system()=='Linux' ): package.close()                        # Kill child process
        
# --------------------------

    def upgrade_packages( self, package_name ):
        '''
        Receives package name and upgrades it.
        '''
        print_up2date, print_collect = True, True
        print_failure, print_success = True, True
        
        cmd         = "{} install --upgrade {}".format( self.pip_cmd, package_name )
        upgrader    = spawn( cmd, timeout=None )

        for package in upgrader:                                        # Read STDOUT of spawned child
            out = package.decode('utf-8').strip('\r\n')                 #   Process
            if( args.verbose ): print( out, end='\n\n' )                #   [INFO] Print
            
            else:
                if( len(out.split()) > 0 ):
                    if( out.split()[0] == "Requirement" and print_up2date ):
                        print( "Package already up-to-date", end='\n\n' )
                        print_up2date = False
                        self.up2date_ctr += 1
                            
                    elif( out.split()[0] == "Collecting" and print_collect ):
                        print( "Collecting latest version", end='\n\n' )
                        print_collect = False
                        print_up2date = False

                    elif( (out.split()[0]).find("Cannot") != -1 or
                          (out.split()[0]).find("Failed") != -1 and print_failure ):
                        print( "{}{}{}".format(str_format.CRED2, out.lstrip(), str_format.CEND), end='\n\n' )
                        print_failure   = False
                        self.failure_ctr += 1
                        
                    elif( out.split()[0] == "Successfully" and print_success ):
                        print( "{}Successfuly upgraded.{}".format(str_format.CGREEN2, str_format.CEND), end='\n\n' )
                        print_success = False
                        self.success_ctr += 1
            
        if( system()=='Linux' ): upgrader.close()                       # Kill child process

# --------------------------

    def stats( self ):
        '''
        Print program stats.
        '''
        len_cte = 40                                                    # Formating constant
        print( "*" * len_cte )                                          # [INFO] Print break lines
        print( "RESULTS:-" )                                            # ...
        print( "  {:5} packages processed".format(self.package_ctr)  )  # ...
        print( "    {:5} already up-to-date".format(self.up2date_ctr))  # ...
        print( "    {:5} successful updates".format(self.success_ctr))  # ...
        print( "    {:5} failed     updates".format(self.failure_ctr))  # ...
        print( "  {:<5.5f} runtime".format(time() - self.start_timer))  # ...
        print( "*" * len_cte )                                          # [INFO] Print break lines

# ************************************************************************
# =========================> MAKE IT ALL HAPPEN <=========================
# ************************************************************************

pip_upgrade()
